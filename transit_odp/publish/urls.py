from django.conf.urls import url

from transit_odp.publish.views import FeedUploadWizard

app_name = 'publish'
urlpatterns = [
    url(r'', FeedUploadWizard.as_view(), name='new'),
]
