from django import forms
from django.core.exceptions import ValidationError

from transit_odp.publish.models import Feed


class FeedNameForm(forms.ModelForm):
    class Meta:
        model = Feed
        fields = ('name',)


class FeedDescriptionForm(forms.ModelForm):
    class Meta:
        model = Feed
        fields = ('description',)


class FeedUploadForm(forms.ModelForm):
    class Meta:
        model = Feed
        fields = ('dataset', 'remote')

    def clean(self):
        cleaned_data = super().clean()
        dataset = cleaned_data.get('dataset', None)
        remote = cleaned_data.get('remote', None)
        if not (bool(dataset) ^ bool(remote)):
            raise ValidationError('You must upload a file or provide a url')
        return cleaned_data
