from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from formtools.wizard.views import SessionWizardView

from transit_odp.publish.form import FeedNameForm, FeedDescriptionForm, FeedUploadForm
from transit_odp.publish.models import Feed


class FeedUploadWizard(SessionWizardView):
    file_storage = FileSystemStorage(location=settings.MEDIA_ROOT)
    form_list = [FeedNameForm, FeedDescriptionForm, FeedUploadForm]
    template_name = 'feed_form.html'

    def done(self, form_list, **kwargs):

        all_data = self.get_all_cleaned_data()

        feed = Feed(**all_data)
        feed.save()

        return render(self.request, 'done.html', {
            'form_data': all_data,
        })
