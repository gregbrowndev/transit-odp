from django.db import models


class Feed(models.Model):
    name = models.CharField(help_text='The name of the feed', max_length=255)
    description = models.TextField(blank=True, help_text='A description of the feed')
    dataset = models.FileField(null=True, blank=True, help_text='The dataset to upload')
    remote = models.URLField(blank=True, help_text='A url to the package')
